
  function playMe(n){
    var allvids = document.getElementsByTagName('video')
    for(var i = 0; i < allvids.length; i++){
      allvids[i].pause();
    }
    var vd = allvids[n];
    vd.play();
  }

  function vids(l){
    for (v=0; v<=l; v++) {
          vCont = document.createElement('video');          
          vCont.src = "http://techslides.com/demos/sample-videos/small.mp4";
          vCont.id = "video_" +v;
          document.querySelector('.myconditions').appendChild(vCont);
          (function(index) {
            document.getElementById("video_"+index).addEventListener("click", function(){
              playMe(index);
            }); 
          })(v);
        }
  }

  function conditions(lati, longi) {
    var xhr = new XMLHttpRequest();
    var url = "http://api.openweathermap.org/data/2.5/weather?lat="+lati+ "&lon=" +longi+"&appid=67d18781c1b7659cbda2e6fbdce595a9";
    
    xhr.open("GET", url, true);
    xhr.send();
    xhr.onreadystatechange = function() {
      if(xhr.readyState == 4 && xhr.status == 200) {
        var json = JSON.parse(xhr.responseText);
        var info = [];
        var iCont; 
        info.push(Math.round(json.main.temp - 273.15), json.weather[0].description, json.wind.speed);

        iRow = document.createElement('div');
        iRow.className = 'myconditions';
        document.querySelector('.myweather').appendChild(iRow);

        for (i=0; i<=info.length-1; i++){
          iCont = document.createElement('div');
          iCont.className = 'condition';
          iCont.innerHTML = info[i];
          document.querySelector('.myconditions').appendChild(iCont);
        } 

        return vids(info.length-1);  
      }      
    }

  }


  function success(position) {
    var mapcanvas = document.createElement('div');
    mapcanvas.id = 'mapcanvas';
    mapcanvas.style.height = '400px';
    document.querySelector('.myweather').appendChild(mapcanvas);

    var coords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    var options = {
      zoom: 15,
      center: coords,
      mapTypeControl: false,  
      navigationControlOptions: {
        style: google.maps.NavigationControlStyle.SMALL
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("mapcanvas"), options);
    var marker = new google.maps.Marker({
      position: coords,
      map: map,
      animation: google.maps.Animation.DROP
    });

    conditions(position.coords.latitude, position.coords.longitude)
  }  

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(success);
  } else {
    alert('Geo Location is not supported');
  }
